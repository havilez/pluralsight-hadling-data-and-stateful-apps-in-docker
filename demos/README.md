# Handling Data and Stateful Applications in Docker

Source code and sample apps for the Pluralsight course.

Each module has its own README listing the samples and the pre-reqs needed to build and run them.

