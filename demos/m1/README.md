# Understanding Storage in Docker Images, Containers and Volumes 

Module 1 samples - simple web apps to demonstrate image layers.

The `linux` samples use Linux containers. You can build and run them with [Docker on Linux](https://hub.docker.com/search?q=&type=edition&offering=community), [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac) and [Docker Desktop for Windows 10](https://hub.docker.com/editions/community/docker-ce-desktop-windows) (in Linux containers mode).

The `windows` samples use Windows containers. You can build and run them on Windows Server 2019 with [Docker Enterprise](https://hub.docker.com/editions/enterprise/docker-ee-server-windows) or [Docker Desktop for Windows 10](https://hub.docker.com/editions/community/docker-ce-desktop-windows) (in Windows containers mode).
