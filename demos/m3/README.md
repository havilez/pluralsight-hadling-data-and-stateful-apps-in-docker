# Using Volume Plugins for Scaling Dynamic Stateful Applications

Module 3 samples - stateful web apps used to demonstrate volume plugins.

The sample apps `api-consumer` and `store` use the same code as Module 2, so you can use the same Docker images.

These apps use Linux containers. You can build and run them with [Docker on Linux](https://hub.docker.com/search?q=&type=edition&offering=community), [Docker Desktop for Mac](https://hub.docker.com/editions/community/docker-ce-desktop-mac) and [Docker Desktop for Windows 10](https://hub.docker.com/editions/community/docker-ce-desktop-windows) (in Linux containers mode).